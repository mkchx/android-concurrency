package com.mkchx.concurrency.interfaces;

import com.mkchx.concurrency.model.GitUser;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GitHubService {
    @GET("users/{user}")
    Call<GitUser> getUser(@Path("user") String user);
}