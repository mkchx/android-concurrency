package com.mkchx.concurrency.task;

import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.Executor;

public class SerialExecutor implements Executor {

    private final Queue mTasks = new ArrayDeque();
    private final Executor mExecutor;
    private Runnable mCurrent;

    public SerialExecutor(Executor executor) {
        mExecutor = executor;
    }

    public synchronized void execute(final Runnable command) {

        mTasks.add(new Runnable() {
            public void run() {
                try {
                    command.run();
                } finally {
                    scheduleNext();
                }
            }
        });

        if (mCurrent == null) {
            scheduleNext();
        }
    }

    protected synchronized void scheduleNext() {
        if ((mCurrent = (Runnable) mTasks.poll()) != null) {
            mExecutor.execute(mCurrent);
        }
    }
}
