package com.mkchx.concurrency.task;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.mkchx.concurrency.activity.MainActivity;
import com.mkchx.concurrency.model.GitUser;

public class AsyncGetData extends AsyncTask<Void, String, GitUser> {

    private MainActivity mainActivity;

    public AsyncGetData(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    protected void onPostExecute(GitUser gitUser) {
        super.onPostExecute(gitUser);

        if (gitUser != null) {
            Log.e("fetchAsyncTask", gitUser.getLogin());

            if (mainActivity != null) {
                Toast.makeText(mainActivity, gitUser.getLogin(), Toast.LENGTH_SHORT).show();
            }
        }

        // TODO: do something, e.g save, populate the data
    }

    @Override
    protected GitUser doInBackground(Void... params) {
        return mainActivity.getGitUser();
    }
}
