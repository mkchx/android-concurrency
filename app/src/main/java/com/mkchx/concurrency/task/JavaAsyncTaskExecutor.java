package com.mkchx.concurrency.task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class JavaAsyncTaskExecutor<T> implements Callable<T> {

    private ExecutorService mExecutorService;
    private ExecutorCompletionService<T> mExecutorCompletionService;

    private List<Callable<T>> mQueue = new ArrayList<>();
    private int mSize = 0;

    public void cancelAll() {
        shutdownAndAwaitTermination(mExecutorService);
    }

    public JavaAsyncTaskExecutor(int poolSize) {

        // .newSingleThreadExecutor(); || pass poolSize of 1 to newFixedThreadPool();
        // .newScheduledThreadPool(10);

        mExecutorService = Executors.newFixedThreadPool(poolSize);
        mExecutorCompletionService = new ExecutorCompletionService<>(mExecutorService);
    }

    public void addTask(Callable<T> task) {
        mQueue.add(task);
    }

    public void begin() {

        mSize = mQueue.size();
        for (Callable<T> task : mQueue) {
            mExecutorCompletionService.submit(task);
        }
    }

    private void shutdownAndAwaitTermination(ExecutorService pool) {
        pool.shutdown(); // Disable new tasks from being submitted
        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
                pool.shutdownNow(); // Cancel currently executing tasks
                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(60, TimeUnit.SECONDS))
                    System.err.println("Pool did not terminate");

            }
        } catch (InterruptedException ie) {
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }
    }

    public List<T> getResults() {

        List<T> results = new ArrayList<>();
        try {

            for (int i = 0; i < mSize; i++) {

                final Future<T> task = mExecutorCompletionService.take();

                if (!mExecutorService.isShutdown() && !task.isCancelled() && task.isDone()) {
                    try {
                        // save the results
                        Thread.sleep(7000);
                        results.add(task.get());

                    } catch (ExecutionException e) {
                        e.printStackTrace();
                    }
                }
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return results;
    }

    @Override
    public T call() throws Exception {
        try {
            final Future<T> task = mExecutorCompletionService.take();
            if (!mExecutorService.isShutdown() && !task.isCancelled() && task.isDone()) {
                try {
                    // TODO: do something with the result
                    return task.get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }
}
