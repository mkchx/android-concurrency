package com.mkchx.concurrency.activity;

import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import com.mkchx.concurrency.R;
import com.mkchx.concurrency.helper.LogicHelper;
import com.mkchx.concurrency.interfaces.GitHubService;
import com.mkchx.concurrency.model.GitUser;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends BaseActivity {

    private Retrofit retrofit;
    private LogicHelper.UiTaskHandler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create a very simple REST adapter which points the GitHub API.
        retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        firstExample();
        secondExample();
        thirdExample();
    }

    private void firstExample() {
        LogicHelper.fetchAsyncTask(this, false);
    }

    private void secondExample() {

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Message completeMessage = mHandler.obtainMessage(1, getGitUser());
                completeMessage.sendToTarget();
            }
        };

        mHandler = new LogicHelper.UiTaskHandler(this);

        List<Runnable> lista = new ArrayList<>();
        lista.add(runnable);

        LogicHelper.fetchWithRunnable(lista);
    }

    private void thirdExample() {

        Callable callable = new Callable() {

            @Override
            public GitUser call() throws Exception {

                // TODO: do something
                return getGitUser();
            }
        };

        List<Callable> lista = new ArrayList<>();
        lista.add(callable);

        LogicHelper.fetchWithCallable(lista);
    }

    public GitUser getGitUser() {

        GitHubService github = retrofit.create(GitHubService.class);
        Call<GitUser> call = github.getUser("mkchx");

        try {
            return call.execute().body();
        } catch (IOException e) {
            Log.e("IOException: ", e.getMessage());
            return null;
        }
    }

}
