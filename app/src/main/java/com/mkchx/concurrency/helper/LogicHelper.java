package com.mkchx.concurrency.helper;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.mkchx.concurrency.activity.MainActivity;
import com.mkchx.concurrency.model.GitUser;
import com.mkchx.concurrency.task.AsyncGetData;
import com.mkchx.concurrency.task.JavaAsyncTaskExecutor;
import com.mkchx.concurrency.task.SerialExecutor;

import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;

public class LogicHelper {

    /**
     * Execute list of tasks in separate thread,
     * we use AsyncTask for that
     *
     * @param parallel
     */
    public static void fetchAsyncTask(MainActivity activity, boolean parallel) {

        AsyncGetData mAsyncGetData = new AsyncGetData(activity);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (parallel) {
                mAsyncGetData.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else {
                mAsyncGetData.executeOnExecutor(AsyncTask.SERIAL_EXECUTOR);
            }
        } else {

            mAsyncGetData.execute();
        }
    }

    /**
     * Java Concurrency
     * Execute list of tasks in separate thread,
     * we use Feature to return the result of our operation.
     * We can terminate any time the operation by calling cancelAll method
     *
     * @param callables list of callables
     */
    public static void fetchWithCallable(List<Callable> callables) {

        final JavaAsyncTaskExecutor javaAsyncTaskExecutor = new JavaAsyncTaskExecutor(1);

        for (Callable callable : callables) {
            javaAsyncTaskExecutor.addTask(callable);
        }

        javaAsyncTaskExecutor.begin();

        new Thread(new Runnable() {
            public void run() {
                List<String> test = javaAsyncTaskExecutor.getResults();
                Log.e("fetchWithCallable", String.valueOf(test.size()));
            }
        }).start();
    }

    /**
     * Java Concurrency
     * Execute list of tasks in separate thread,
     * we use Runnable to notify the task completion
     * We cannot terminate the tasks
     *
     * @param runnables list of runnables
     */
    public static void fetchWithRunnable(List<Runnable> runnables) {

        SerialExecutor executor = new SerialExecutor(Executors.newSingleThreadExecutor());

        for (Runnable runnable : runnables) {
            executor.execute(runnable);
        }
    }

    public static class UiTaskHandler extends Handler {

        private final WeakReference<MainActivity> mActivity;

        public UiTaskHandler(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public synchronized void handleMessage(Message msg) {

            MainActivity activity = mActivity.get();
            if (activity != null) {
                GitUser user = (GitUser) msg.obj;

                if (user != null) {
                    Log.e("fetchWithRunnable", user.getLogin());
                    Toast.makeText(activity, user.getLogin(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
