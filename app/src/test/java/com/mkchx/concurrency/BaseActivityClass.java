package com.mkchx.concurrency;

import android.content.Context;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@RunWith(MockitoJUnitRunner.class)
public class BaseActivityClass {

    public final String API_URL = "https://api.github.com";
    protected Retrofit retrofit;

    @Mock
    Context mMockContext;

    @Before
    public void setUp() throws Exception {

        // Create a very simple REST adapter which points the GitHub API.
        retrofit = new Retrofit.Builder()
                .baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @After
    public void tearDown() throws Exception {
        retrofit = null;
    }
}
