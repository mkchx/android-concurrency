package com.mkchx.concurrency;

import com.mkchx.concurrency.interfaces.GitHubService;
import com.mkchx.concurrency.model.GitUser;

import org.junit.Test;

import java.io.IOException;

import retrofit2.Call;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class MainActivityTest extends BaseActivityClass {

    @Test
    public void getApiResult() {
        try {

            GitHubService github = retrofit.create(GitHubService.class);
            Call<GitUser> call = github.getUser("mkchx");

            GitUser user = call.execute().body();

            assertThat(user, notNullValue());
            assertThat(user.getLogin(), is("mkchx"));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
